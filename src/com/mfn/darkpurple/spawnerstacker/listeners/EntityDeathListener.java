package com.mfn.darkpurple.spawnerstacker.listeners;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.metadata.FixedMetadataValue;

import com.mfn.darkpurple.spawnerstacker.SpawnerStacker;
import com.mfn.darkpurple.spawnerstacker.config.ConfigHelper;
import com.mfn.darkpurple.spawnerstacker.spawners.MobStackHandler;
import com.mfn.darkpurple.spawnerstacker.spawners.MobStackWorld;

public class EntityDeathListener implements Listener {
	
	private final MobStackHandler stackHandler;
	private final SpawnerStacker plugin;
	private final ConfigHelper config;
	
	public EntityDeathListener(MobStackHandler stackHandler, SpawnerStacker plugin) {
		this.stackHandler = stackHandler;
		this.plugin = plugin;
		config = plugin.getSpawnerStackerConfig();
	}
	
	@EventHandler
	public void death(EntityDeathEvent e) {
		
		LivingEntity stack = e.getEntity();
		
		// only handle stack entities
		if (!stack.hasMetadata(SpawnerStacker.STACK_ID_METADATA)) return;
		if (stack.hasMetadata(SpawnerStacker.STACK_DEAD_METADATA)) return; // don't handle entities marked as dead
		final long id = stack.getMetadata(SpawnerStacker.STACK_ID_METADATA).get(0).asLong();

		// calculate remaining damage on next stack entities
		final double remainingDamage = stack.hasMetadata(SpawnerStacker.STACK_REMAINING_DAMAGE) ? stack.getMetadata(SpawnerStacker.STACK_REMAINING_DAMAGE).get(0).asDouble() : 0.0;
		
		// get values from stack to give to new stack
		// don't carry over velocity because CrazyEnchantments throws errors
		final int fireTicks = stack.getFireTicks();
		final double absorptionAmount = stack.getAbsorptionAmount();
		final boolean pickupItems = stack.getCanPickupItems();
		final boolean collidable = stack.isCollidable();
		final String customName = stack.getCustomName();
		final boolean customNameVisible = stack.isCustomNameVisible();
		final boolean glowing = stack.isGlowing();
		final boolean leashed = stack.isLeashed();
		final Entity leashHolder = leashed ? stack.getLeashHolder() : null;
		final int portalCooldown = stack.getPortalCooldown();
		final Location location = stack.getLocation();
		final boolean swimming = stack.isSwimming();
		final Player killer = stack.getKiller();

		final MobStackWorld stackWorld = stackHandler.getWorld(stack.getWorld());
		final World world = stack.getWorld();
		final EntityType type = stack.getType();
		
		// remove an entity from the stack
		final long amount = stackWorld.removeFromStack(stack);
		if (amount == 0) return;

		// check whether stack deaths has reached maxDeaths
		long deaths = 1;
		if (stack.hasMetadata(SpawnerStacker.STACK_DEATH_AMOUNT)) deaths = stack.getMetadata(SpawnerStacker.STACK_DEATH_AMOUNT).get(0).asLong() + 1;
		
		// spawn a new stack because the old one died, and transfer the old values over
		stack = (LivingEntity) world.spawnEntity(location, type);
		stack.setMetadata(SpawnerStacker.STACK_ID_METADATA, new FixedMetadataValue(plugin, id));
		stack.setFireTicks(fireTicks);
		stack.setAbsorptionAmount(absorptionAmount);
		stack.setCanPickupItems(pickupItems);
		stack.setCollidable(collidable);
		if (customName != null) {
			stack.setCustomName(customName);
			stack.setCustomNameVisible(customNameVisible);
		}
		stack.setGlowing(glowing);
		if (leashed) stack.setLeashHolder(leashHolder);
		stack.setPortalCooldown(portalCooldown);
		stack.setSwimming(swimming);

		// update the stored stack entity for this ID
		stackWorld.updateStack(id, stack);
		
		// if deaths is above maxDeaths, reset stack death amount and return
		if (deaths >= config.getMaxDeaths()) {
			stack.removeMetadata(SpawnerStacker.STACK_DEATH_AMOUNT, plugin);
			return;
		}
		
		// set death amount on new stack
		stack.setMetadata(SpawnerStacker.STACK_DEATH_AMOUNT, new FixedMetadataValue(plugin, deaths));
		
		// damage the new stack by the remaining damage, which goes back through EntityDamageListener
		if (remainingDamage > 0.0) {
			if (killer == null) stack.damage(remainingDamage);
			else stack.damage(remainingDamage, killer);
		}
		
	}
	
}
