package com.mfn.darkpurple.spawnerstacker.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldLoadEvent;

import com.mfn.darkpurple.spawnerstacker.spawners.MobStackHandler;

public class WorldLoadListener implements Listener {
	
	private final MobStackHandler stackHandler;
	
	public WorldLoadListener(MobStackHandler stackHandler) {
		this.stackHandler = stackHandler;
	}
	
	@EventHandler
	public void load(WorldLoadEvent e) {
		stackHandler.addWorld(e.getWorld());
	}
	
}
