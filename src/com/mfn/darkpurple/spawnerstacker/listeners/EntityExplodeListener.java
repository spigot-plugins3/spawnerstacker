package com.mfn.darkpurple.spawnerstacker.listeners;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

import com.mfn.darkpurple.spawnerstacker.SpawnerStacker;
import com.mfn.darkpurple.spawnerstacker.spawners.MobStackHandler;

public class EntityExplodeListener implements Listener {
	
	private final MobStackHandler stackHandler;
	
	public EntityExplodeListener(MobStackHandler stackHandler) {
		this.stackHandler = stackHandler;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void explode(EntityExplodeEvent e) {
		if (!(e.getEntity() instanceof LivingEntity)) return;
		
		final LivingEntity stack = (LivingEntity) e.getEntity();
		if (!e.getEntity().hasMetadata(SpawnerStacker.STACK_ID_METADATA)) return;
		
		// remove creeper stacks when they explode so they don't leave phantom armour stands
		stackHandler.getWorld(stack.getWorld()).removeAllFromStack(stack);
		
	}
	
}
