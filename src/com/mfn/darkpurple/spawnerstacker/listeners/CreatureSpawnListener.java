package com.mfn.darkpurple.spawnerstacker.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import com.mfn.darkpurple.spawnerstacker.spawners.MobStackHandler;

public class CreatureSpawnListener implements Listener {
	
	private final MobStackHandler stackHandler;
	
	public CreatureSpawnListener(MobStackHandler stackHandler) {
		this.stackHandler = stackHandler;
	}
	
	@EventHandler
	public void spawn(CreatureSpawnEvent e) {
		if (e.getSpawnReason() != SpawnReason.SPAWNER) return; // only handle mob spawner mobs
		
		// add to stack
		stackHandler.getWorld(e.getLocation().getWorld()).addToStack(e.getEntity());
		
	}
	
}
