package com.mfn.darkpurple.spawnerstacker.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldUnloadEvent;

import com.mfn.darkpurple.spawnerstacker.spawners.MobStackHandler;

public class WorldUnloadListener implements Listener {
	
	private final MobStackHandler stackHandler;
	
	public WorldUnloadListener(MobStackHandler stackHandler) {
		this.stackHandler = stackHandler;
	}
	
	@EventHandler
	public void unload(WorldUnloadEvent e) {
		stackHandler.removeWorld(e.getWorld());
	}
	
}
