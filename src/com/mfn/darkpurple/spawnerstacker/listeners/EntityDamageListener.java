package com.mfn.darkpurple.spawnerstacker.listeners;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;

import com.mfn.darkpurple.spawnerstacker.SpawnerStacker;

public class EntityDamageListener implements Listener {
	
	private final Plugin plugin;
	
	public EntityDamageListener(Plugin plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void damage(EntityDamageEvent e) {
		if (e.isCancelled()) return;
		if (!(e.getEntity() instanceof LivingEntity)) return;
		
		final LivingEntity entity = (LivingEntity) e.getEntity();
		if (!entity.hasMetadata(SpawnerStacker.STACK_ID_METADATA)) return; // only handle entities that are stacked by this plugin
		
		// set remaining damage so EntityDeathListener can process it
		if (entity.getHealth() - e.getFinalDamage() < 0.0) entity.setMetadata(SpawnerStacker.STACK_REMAINING_DAMAGE, new FixedMetadataValue(plugin, e.getFinalDamage() - entity.getHealth()));
		
	}
	
}
