package com.mfn.darkpurple.spawnerstacker.listeners;

import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.plugin.Plugin;

import com.mfn.darkpurple.spawnerstacker.SpawnerStacker;

import net.md_5.bungee.api.ChatColor;

public class ChunkLoadListener implements Listener {
	
	final Plugin plugin;
	
	public ChunkLoadListener(Plugin plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void load(ChunkLoadEvent e) {
		if (e.isNewChunk()) return; // there are no mob stacks in new chunks
		
		for (Entity entity : e.getChunk().getEntities()) {
			if (!(entity instanceof ArmorStand)) continue;
			if (entity.hasMetadata(SpawnerStacker.STACK_ARMOUR_STAND_METADATA)) continue; // armour stand is currently in use
			if (entity.getCustomName() == null || !entity.getCustomName().startsWith(ChatColor.AQUA + "Stack: ")) continue; // armour stand was not made by this plugin
			
			entity.remove();
			
		}
		
	}
	
}
