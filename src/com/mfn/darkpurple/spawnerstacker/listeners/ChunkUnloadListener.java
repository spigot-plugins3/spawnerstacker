package com.mfn.darkpurple.spawnerstacker.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

import com.mfn.darkpurple.spawnerstacker.SpawnerStacker;
import com.mfn.darkpurple.spawnerstacker.spawners.MobStackHandler;
import com.mfn.darkpurple.spawnerstacker.spawners.MobStackWorld;

public class ChunkUnloadListener implements Listener {
	
	private final MobStackHandler stackHandler;
	
	public ChunkUnloadListener(MobStackHandler stackHandler) {
		this.stackHandler = stackHandler;
	}
	
	@EventHandler
	public void unload(ChunkUnloadEvent e) {
		
		final MobStackWorld stackWorld = stackHandler.getWorld(e.getWorld());
		
		for (Entity entity : e.getChunk().getEntities()) {
			if (!entity.hasMetadata(SpawnerStacker.STACK_ID_METADATA)) continue;
			
			stackWorld.removeAllFromStack((LivingEntity) entity);
			
		}
		
	}
	
}
