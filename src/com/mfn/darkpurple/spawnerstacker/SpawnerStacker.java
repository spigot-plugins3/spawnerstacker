package com.mfn.darkpurple.spawnerstacker;

import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import com.mfn.darkpurple.spawnerstacker.config.ConfigHelper;
import com.mfn.darkpurple.spawnerstacker.listeners.ChunkLoadListener;
import com.mfn.darkpurple.spawnerstacker.listeners.ChunkUnloadListener;
import com.mfn.darkpurple.spawnerstacker.listeners.CreatureSpawnListener;
import com.mfn.darkpurple.spawnerstacker.listeners.EntityDamageListener;
import com.mfn.darkpurple.spawnerstacker.listeners.EntityDeathListener;
import com.mfn.darkpurple.spawnerstacker.listeners.EntityExplodeListener;
import com.mfn.darkpurple.spawnerstacker.listeners.WorldLoadListener;
import com.mfn.darkpurple.spawnerstacker.listeners.WorldUnloadListener;
import com.mfn.darkpurple.spawnerstacker.spawners.MobStack;
import com.mfn.darkpurple.spawnerstacker.spawners.MobStackHandler;
import com.mfn.darkpurple.spawnerstacker.spawners.MobStackWorld;

public class SpawnerStacker extends JavaPlugin {
	
	public static final String STACK_ID_METADATA = "stack_id";
	public static final String STACK_REMAINING_DAMAGE = "stack_damage";
	public static final String STACK_DEAD_METADATA = "stack_dead";
	public static final String STACK_ARMOUR_STAND_METADATA = "stack_name";
	public static final String STACK_DEATH_AMOUNT = "stack_deaths";
	public static FixedMetadataValue STACK_ARMOUR_STAND_METADATA_VALUE;
	
	private ConfigHelper config = null;
	private MobStackHandler stackHandler = new MobStackHandler(this);
	
	public SpawnerStacker() {
		STACK_ARMOUR_STAND_METADATA_VALUE = new FixedMetadataValue(this, null);
	}
	
	public static FixedMetadataValue getArmourStandMetadataValue() { return STACK_ARMOUR_STAND_METADATA_VALUE; }
	
	@Override
	public void onEnable() {
		final long start = System.currentTimeMillis();
		
		saveDefaultConfig();
		config = new ConfigHelper(getDataFolder());
		
		// add worlds that loaded during startup to stack handler
		for (World world : getServer().getWorlds()) stackHandler.addWorld(world);

		// register events
		getServer().getPluginManager().registerEvents(new ChunkLoadListener(this), this);
		getServer().getPluginManager().registerEvents(new ChunkUnloadListener(stackHandler), this);
		getServer().getPluginManager().registerEvents(new CreatureSpawnListener(stackHandler), this);
		getServer().getPluginManager().registerEvents(new EntityDamageListener(this), this);
		getServer().getPluginManager().registerEvents(new EntityDeathListener(stackHandler, this), this);
		getServer().getPluginManager().registerEvents(new EntityExplodeListener(stackHandler), this);
		getServer().getPluginManager().registerEvents(new WorldLoadListener(stackHandler), this);
		getServer().getPluginManager().registerEvents(new WorldUnloadListener(stackHandler), this);
		
		// every tick move the armour stands in line with the stacked entities
		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			MobStack mobStack;
			Location location;
			public void run() {
				
				for (MobStackWorld world : stackHandler.getWorlds()) {
					for (Entry<EntityType, Map<Long, MobStack>> types : world.getMobStacks().entrySet()) {
						for (Entry<Long, MobStack> stack : types.getValue().entrySet()) {
							
							mobStack = stack.getValue();
							location = mobStack.getStack().getLocation();
							
							mobStack.getArmorStand().teleport(new Location(location.getWorld(), location.getX(), location.getY() + mobStack.getStack().getHeight(), location.getZ()));
							
						}
					}
				}
				
			}
		}, 0, 1);
		
		getLogger().info("Started in " + (System.currentTimeMillis() - start) + "ms");
	}
	
	@Override
	public void onDisable() {
		
		// remove all armour stands and metadata because they can persist through restarts otherwise
		long removed = stackHandler.removeAllStacks();
		
		getLogger().info("Removed " + removed + " stacks.");
		
	}
	
	public ConfigHelper getSpawnerStackerConfig() { return config; }
	
}
