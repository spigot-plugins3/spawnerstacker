package com.mfn.darkpurple.spawnerstacker.config;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigHelper {
	
	private final String CONFIG_FILE = "config.yml";
	private final File pluginFolder;
	
	private final String MERGE_DISTANCE_KEY = "mergeDistance";
	private final String MAX_DEATHS_KEY = "maxDeaths";
	
	private double mergeDistance = 20.0;
	private long maxDeaths = 20;
	
	public ConfigHelper(File pluginFolder) {
		this.pluginFolder = pluginFolder;
		reload();
	}
	
	public void reload() {
		FileConfiguration config = YamlConfiguration.loadConfiguration(new File(pluginFolder.toString() + "/" + CONFIG_FILE));
		
		mergeDistance = config.getDouble(MERGE_DISTANCE_KEY);
		maxDeaths = config.getLong(MAX_DEATHS_KEY);
	}
	
	public double getMergeDistance() { return mergeDistance; }
	public long getMaxDeaths() { return maxDeaths; }
	
}
