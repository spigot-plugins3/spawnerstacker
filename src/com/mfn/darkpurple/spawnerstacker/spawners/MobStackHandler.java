package com.mfn.darkpurple.spawnerstacker.spawners;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

import com.mfn.darkpurple.spawnerstacker.SpawnerStacker;

public class MobStackHandler {
	
	private SpawnerStacker plugin;
	
	public MobStackHandler(SpawnerStacker plugin) {
		this.plugin = plugin;
	}
	
	private Map<World, MobStackWorld> worlds = new HashMap<>();
	
	public void addWorld(World world) { if (!worlds.containsKey(world)) worlds.put(world, new MobStackWorld(plugin)); }
	public void removeWorld(World world) { worlds.remove(world); }
	
	public MobStackWorld getWorld(World world) { return worlds.get(world); }
	public Collection<MobStackWorld> getWorlds() { return worlds.values(); }
	
	public LivingEntity getMobStackInRange(EntityType type, Location location) {
		final World world = location.getWorld();
		if (!worlds.containsKey(world)) return null;
		return worlds.get(world).getMobStackInRange(type, location);
	}
	
	public long removeAllStacks() {
		long removed = 0;
		
		for (World world : worlds.keySet()) {
			for (Entity entity : world.getEntities()) {
				
				if (entity.hasMetadata(SpawnerStacker.STACK_ID_METADATA)) {
					entity.removeMetadata(SpawnerStacker.STACK_ID_METADATA, plugin);
					removed++;
				}
				else if (entity.hasMetadata(SpawnerStacker.STACK_ARMOUR_STAND_METADATA)) entity.remove();
				
			}
		}
		
		return removed;
	}
	
}
