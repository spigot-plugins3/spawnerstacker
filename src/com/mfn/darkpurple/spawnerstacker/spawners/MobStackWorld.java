package com.mfn.darkpurple.spawnerstacker.spawners;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.metadata.FixedMetadataValue;

import com.mfn.darkpurple.spawnerstacker.SpawnerStacker;

public class MobStackWorld {
	
	private final SpawnerStacker plugin;
	private final Map<EntityType, Map<Long, MobStack>> stacks = new HashMap<>();
	
	public MobStackWorld(SpawnerStacker plugin) {
		this.plugin = plugin;
	}
	
	private double distanceBetweenLocations(Location p1, Location p2) {
		
		final double x = p2.getX() - p1.getX();
		final double y = p2.getY() - p1.getY();
		final double z = p2.getZ() - p1.getZ();
		
		return Math.sqrt((x * x) + (y * y) + (z * z));
	}
	
	public LivingEntity getMobStackInRange(EntityType type, Location location) {
		
		if (!stacks.containsKey(type)) return null;
		
		final double mergeDistance = plugin.getSpawnerStackerConfig().getMergeDistance();
		double distance;
		for (Entry<Long, MobStack> entry : stacks.get(type).entrySet()) {
			final LivingEntity entity = entry.getValue().getStack();
			
			distance = Math.abs(distanceBetweenLocations(location, entity.getLocation()));
			
			if (distance <= mergeDistance) return entity;
		}
		
		return null;
	}
	
	public Map<EntityType, Map<Long, MobStack>> getMobStacks() { return stacks; }
	
	public long addToStack(LivingEntity entity) {
		
		final EntityType type = entity.getType();
		final Location entityLocation = entity.getLocation();
		LivingEntity stack = getMobStackInRange(type, entityLocation);
		
		long id = System.currentTimeMillis();
		long amount = 1;
		if (stack == null) { // stack doesn't exist in range, this entity is now the new stack
			
			entity.setMetadata(SpawnerStacker.STACK_ID_METADATA, new FixedMetadataValue(plugin, id)); // generate a random ID for this stack
			entity.setRemoveWhenFarAway(false); // don't despawn the stack when chunks are unloaded, causing phantom armour stands

			if (!stacks.containsKey(type)) stacks.put(type, new HashMap<>());
			stacks.get(type).put(id, new MobStack(entity));
			
		} else { // stack exists in range, add 1 to that stack and remove this entity
			
			id = stack.getMetadata(SpawnerStacker.STACK_ID_METADATA).get(0).asLong();
			stacks.get(type).get(id).add();
			
			entity.remove();
			
		}
		
		return amount;
	}
	
	public void updateStack(long id, LivingEntity entity) {
		final MobStack stack = stacks.get(entity.getType()).get(id);
		stack.setEntity(entity);
	}
	
	public long removeAllFromStack(LivingEntity stack) { return removeFromStack(stack, stacks.get(stack.getType()).get(stack.getMetadata(SpawnerStacker.STACK_ID_METADATA).get(0).asLong()).getAmount()); }
	public long removeFromStack(LivingEntity stack) { return removeFromStack(stack, 1); }
	public long removeFromStack(LivingEntity stack, long amount) {
		
		final EntityType type = stack.getType();
		final long id = stack.getMetadata(SpawnerStacker.STACK_ID_METADATA).get(0).asLong();
		
		final MobStack mobStack = stacks.get(type).get(id);
		final long remaining = mobStack.remove(amount);
		
		if (remaining <= 0) {
			
			// no more mobs left in the stack, remove the armour stand and the stack will die naturally
			mobStack.getArmorStand().remove();
			stacks.get(type).remove(id);
			
			if (stacks.get(type).size() == 0) stacks.remove(type);
			
		}
		
		return remaining;
	}
	
}
