package com.mfn.darkpurple.spawnerstacker.spawners;

import org.bukkit.Location;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;

import com.mfn.darkpurple.spawnerstacker.SpawnerStacker;

import net.md_5.bungee.api.ChatColor;

public class MobStack {
	
	private LivingEntity stack;
	private final ArmorStand armourStand;
	
	private long amount = 1;
	
	public MobStack(LivingEntity stack) {
		this.stack = stack;
		
		armourStand = (ArmorStand) stack.getWorld().spawnEntity(new Location(stack.getWorld(), 0, -1, 0), EntityType.ARMOR_STAND); // spawn stack below the world so nobody sees it momentarily before it disappears
		armourStand.setVisible(false);
		armourStand.setCollidable(false);
		armourStand.setInvulnerable(true);
		armourStand.setCustomNameVisible(true);
		armourStand.setMarker(true); // set small collision box so people can actually damage the stack
		armourStand.setRemoveWhenFarAway(false);
		armourStand.setMetadata(SpawnerStacker.STACK_ARMOUR_STAND_METADATA, SpawnerStacker.getArmourStandMetadataValue());
		
		updateStackAmount();
	}
	
	public LivingEntity getStack() { return stack; }
	public ArmorStand getArmorStand() { return armourStand; }
	public void setEntity(LivingEntity stack) { this.stack = stack; }
	
	public long add() {
		amount += 1;
		updateStackAmount();
		return amount;
	}
	public long remove() { return remove(1); }
	public long remove(long amount) {
		this.amount -= amount;
		updateStackAmount();
		return this.amount;
	}
	
	public long getAmount() { return amount; }
	
	private void updateStackAmount() {
		
		armourStand.setCustomName(ChatColor.AQUA + "Stack: " + amount);
		
	}
	
}
